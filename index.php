<!DOCTYPE html>
<html>
	<head lang="ru-RU">
		<meta charset="utf-8">
		<meta name="description" content="">
		<title>Менеджер</title>
		<meta name="viewport" content="width=device-width">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="favicon.png" type="image/png">
		<link rel="icon" href="favicon.png" type="image/png">
		<link rel="stylesheet" href="http://cdn.jsdelivr.net/webshim/1.14.5/shims/styles/shim.css">
		<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
		<link rel="stylesheet" type="text/css" href="style/s.php/sass.scss">
		
	</head>
	<body>
		<header id="header">
			
		</header>
		<div id="content">
		
		</div>

		<div id="loader-wrapper">
			<div id="loader"></div>

			<div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
            <div class="content_loader">
				
			</div>
		</div>

		<script data-main="js/main" src="js/lib/require.js"></script>
	</body>
</html>